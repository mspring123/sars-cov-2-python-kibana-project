#!/usr/bin/env python

"""
Queries data from the RKI interface, transforms them and sends them to Elasticsearch. No extra Elasticsearch client is required. The data is structured as such that it can be inserted directly via the _bulk-api.

# structure

{
  "type" : "FeatureCollection", 
  "features" : [
    {
      "type" : "Feature", 
      "id" : 168841, 
      "geometry" : null, 
      "properties" : {
        "IdBundesland" : 5, 
        "Bundesland" : "Nordrhein-Westfalen", 
        "Landkreis" : "SK Essen", 
        "Altersgruppe" : "A35-A59", 
        "Geschlecht" : "M", 
        "AnzahlFall" : 5, 
        "AnzahlTodesfall" : 0, 
        "ObjectId" : 168841, 
        "Meldedatum" : 1612396800000, 
        "IdLandkreis" : "05113", 
        "Datenstand" : "21.02.2021, 00:00 Uhr", 
        "NeuerFall" : 0, 
        "NeuerTodesfall" : -9, 
        "Refdatum" : 1612224000000, 
        "NeuGenesen" : 0, 
        "AnzahlGenesen" : 5, 
        "IstErkrankungsbeginn" : 1, 
        "Altersgruppe2" : "Nicht übermittelt"
      }
    }, 
    {
      "type" : "Feature", 
      "id" : 172244, 
      "geometry" : null, 
      "properties" : {
        "IdBundesland" : 5, 
        "Bundesland" : "Nordrhein-Westfalen", 
        "Landkreis" : "SK Essen", 
        "Altersgruppe" : "A35-A59", 
        "Geschlecht" : "W", 
        "AnzahlFall" : 1, 
        "AnzahlTodesfall" : 0, 
        "ObjectId" : 172244, 
        "Meldedatum" : 1601596800000, 
        "IdLandkreis" : "05113", 
        "Datenstand" : "21.02.2021, 00:00 Uhr", 
        "NeuerFall" : 0, 
        "NeuerTodesfall" : -9, 
        "Refdatum" : 1601424000000, 
        "NeuGenesen" : 0, 
        "AnzahlGenesen" : 1, 
        "IstErkrankungsbeginn" : 1, 
        "Altersgruppe2" : "Nicht übermittelt"
      }
    }
  ]
}

"""

# to handle  data retrieval
import urllib3
import requests
# from urllib3 import request
# to handle certificate verification
import certifi
# to manage json data
import json
# for pandas dataframes
import pandas as pd

# uncomment below if installation needed (not necessary in Colab)
#!pip install certifi

#https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/RKI_COVID19/FeatureServer/0

download_link_test = 'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_COVID19/FeatureServer/0/query?where=1%3D1&objectIds=168841%2C+%09172244&time=&resultType=none&outFields=*&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&sqlFormat=none&f=pgeojson&token='

download_link = 'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_COVID19/FeatureServer/0/query?where=1%3D1&objectIds=&time=&resultType=none&outFields=*&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&sqlFormat=none&f=pgeojson&token='

myUsername = "" # Placeholder, no username is required
myPassword = "" # Placeholder, no password is required

response = requests.get(download_link, auth=(myUsername, myPassword),  headers={'User-Agent': 'Mozilla'})
if response.status_code is not 200:
      # This means something went wrong.
      raise ApiError('GET /tasks/ {}'.format(response.status_code))
else:
      print(response.headers['content-type'])
      if response.status_code is 200:
#             features = response.content
#             print(features)
            content = json.loads(response.content.decode('utf-8')) # the whole nested content
            
from pandas.io.json import json_normalize
# print(content['features'])

# in this dataset, the data to extract is under 'features'
from pandas.io.json import json_normalize
# features = json_normalize(content['features'])
features = pd.DataFrame(json_normalize(content['features']))

# features.to_csv('/Users/asdf/Desktop/asdf.csv', index = False, encoding='utf-8-sig')

# Additional column names are assigned during normalization; these are overwritten with this.
features.columns = features.columns.str.replace('properties.','')

# iterating the columns 
# for column in features.columns: 
#       print(column)
# for index, row in features.iterrows():
#       print(int(row['id']), str(row['Meldedatum']), int(row['ObjectId']))

# https://towardsdatascience.com/exporting-pandas-data-to-elasticsearch-724aa4dd8f62
import numpy as np
import datetime

# https://stackoverflow.com/questions/33034559/how-to-remove-last-the-two-digits-in-a-column-that-is-of-integer-type
# properties.Meldedatum

features['Meldedatum'] = features['Meldedatum'].astype(str).str[:-3].astype(np.int64)
features['Meldedatum'] = pd.to_datetime(features['Meldedatum'],unit='s')
# properties.Refdatum
features['Refdatum'] = features['Refdatum'].astype(str).str[:-3].astype(np.int64)
# https://currentmillis.com/
features['Refdatum'] = pd.to_datetime(features['Refdatum'],unit='s')

# for index, row in features.iterrows():
#       print(int(row['id']), row['Meldedatum'], row['Refdatum'])
      
# print(features.info())

from datetime import datetime

def safe_date(date_value):
      return (
            pd.to_datetime(date_value) if not pd.isna(date_value)
                  else  datetime(1970,1,1,0,0)
      )
      
features['Meldedatum'] = features['Meldedatum'].apply(safe_date)
features['Refdatum'] = features['Refdatum'].apply(safe_date)

# print(features.to_json(date_format='iso'))

import requests
import json

url = 'http://localhost:9200/asdf?pretty'
body = {
  "settings": {
    "number_of_shards": 3,
    "number_of_replicas": 2
  }
}


# Test area
def insert_single():
      json_q = json.dumps({"asdf": int(12), "name" : "bob"})
      url = "http://localhost:9200/single-doc/_doc"
      headers ={'Content-Type' : 'application/json'}
      requests.post(url, headers = headers,  data = json_q)  # <-- specify content type
      print('single document inserted')
# insert_single()

def send_to_elastic(elastic_address, endpoint, to_elastic_string, count):
      headers ={'Content-Type' : 'application/json'}
      url = str(elastic_address + endpoint)
      try:
            catcher = requests.post(url, headers = headers, data = to_elastic_string)
            print(catcher, "Inserted documents: " + str(int(count)))
      except:
            print('Error occurred in send_to_elastic!')


elastic_address = "http://localhost:9200"
headers ={'Content-Type' : 'application/json'}
endpoint = "/_bulk?pretty"
to_elastic_string = ""
target_index = "arcgis"
target_type = "services.arcgis"

count = 0
for index, row in features.iterrows():
      index_row = { "index": { "_index" : target_index, "_type" : target_type,"_id": int(row['id'])}}
      json_string = json.dumps(index_row) + "\n" + row.to_json(date_format='iso') + "\n"
      to_elastic_string += json_string
      print(to_elastic_string)
      count += 1
      if count % 200 == 0:
            try:
                  send_to_elastic(elastic_address, endpoint, to_elastic_string, count)
                  to_elastic_string = ""
            except:
                  print('damn it, we have a problem')
# successfully inserted into Elasticsearch
print('documents were successfully inserted into elasticsearch.')
# if Response.status != 200:
#             print ("\n*** Error occured before import. ***")
#             print ("HTTP error code {} / {}".format(response.status, response.reason))
# elif Response.status == 200:
#             print("\n*** Data was transferred successfully. ***")
"""
{"index": {"_type": "services.arcgis", "_id": 452579, "_index": "arcgis"}}
{"geometry":null,"id":452579,"Altersgruppe":"A60-A79","Altersgruppe2":"Nicht \u00fcbermittelt","AnzahlFall":1,"AnzahlGenesen":1,"AnzahlTodesfall":0,"Bundesland":"Rheinland-Pfalz","Datenstand":"27.01.2021, 00:00 Uhr","Geschlecht":"M","IdBundesland":7,"IdLandkreis":"07140","IstErkrankungsbeginn":0,"Landkreis":"LK Rhein-Hunsr\u00fcck-Kreis","Meldedatum":"2020-03-14T00:00:00.000Z","NeuGenesen":0,"NeuerFall":0,"NeuerTodesfall":-9,"ObjectId":452579,"Refdatum":"2020-03-14T00:00:00.000Z","type":"Feature"}
{"index": {"_type": "services.arcgis", "_id": 452934, "_index": "arcgis"}}
{"geometry":null,"id":452934,"Altersgruppe":"A80+","Altersgruppe2":"Nicht \u00fcbermittelt","AnzahlFall":1,"AnzahlGenesen":1,"AnzahlTodesfall":0,"Bundesland":"Rheinland-Pfalz","Datenstand":"27.01.2021, 00:00 Uhr","Geschlecht":"W","IdBundesland":7,"IdLandkreis":"07140","IstErkrankungsbeginn":0,"Landkreis":"LK Rhein-Hunsr\u00fcck-Kreis","Meldedatum":"2020-03-14T00:00:00.000Z","NeuGenesen":0,"NeuerFall":0,"NeuerTodesfall":-9,"ObjectId":452934,"Refdatum":"2020-03-14T00:00:00.000Z","type":"Feature"}
"""
