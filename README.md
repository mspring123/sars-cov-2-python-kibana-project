# Sars-CoV-2 python kibana project


> Author *`michaelspring123@web.de`*
>> Creation date *`2020-12-30`*

# background
The following project is a script collection of python scripts that collect information about sars-cov-2 from various sources and display it visually in Kibana.


# Process diagram of the data transfer
The procedure is the same for all sources. Data is collected by *`web services`*, transformed into python and then sent to elasticsearch via the *`/_bulk-api`* endpoint.

```mermaid
graph LR
    A(collect data set) -->|transform data| B((python)) -->|Load data| C((Elastic Search))
    C --> D{Data exploration}
    D -->|Kibana| E[Queries in kql]
    D -->|Kibana| F[Visualize the data]
```


![Bildschirmvideo_aufnehmen_2021-01-30_um_13.36.31.mov](/pomber-service/kibana-images/Bildschirmvideo_aufnehmen_2021-01-30_um_13.36.31.mov)


# Countries with the highest Corona values
![Screenshot_30_01_21__00_14.jpg](/pomber-service/kibana-images/Screenshot_30_01_21__00_14.jpg)

# Time series of the positively tested cases
![Screenshot_30_01_21__00_15.jpg](/pomber-service/kibana-images/Screenshot_30_01_21__00_15.jpg)

# Excerpt of the age groups in Germany
![Screenshot_30_01_21__00_22.jpg](/arcgis-service/arcgis-images/Screenshot_30_01_21__00_22.jpg)


# Breakdown of the age groups
![Screenshot_30_01_21__00_24.jpg](/arcgis-service/arcgis-images/Screenshot_30_01_21__00_24.jpg)
