#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Transforms the data from CSSEGISandData/COVID-19 into a json file. Available at https://pomber.github.io/covid19/timeseries.json. Updated three times a day using GitHub Actions.

The json contains the number of Coronavirus confirmed cases, deaths, and recovered cases for every country and every day since 2020-1-22

# source
https://github.com/pomber/covid19

# structure

{
  "Afghanistan": [
    {
      "date": "2020-1-22",
      "confirmed": 0,
      "deaths": 0,
      "recovered": 0
    },
    {
      "date": "2020-1-23",
      "confirmed": 0,
      "deaths": 0,
      "recovered": 0
    }
"""

# to handle  data retrieval
import urllib3
import requests
# from urllib3 import request
# to handle certificate verification
import certifi
# to manage json data
import json
# for pandas dataframes
import pandas as pd

# uncomment below if installation needed (not necessary in Colab)
#!pip install certifi

download_link = 'https://pomber.github.io/covid19/timeseries.json'

myUsername = "" # Placeholder, no username is required
myPassword = "" # Placeholder, no password is required

response = requests.get(download_link, auth=(myUsername, myPassword),  headers={'User-Agent': 'Mozilla'})
if response.status_code is not 200:
      # This means something went wrong.
      raise ApiError('GET /tasks/ {}'.format(response.status_code))
else:
      print(response.headers['content-type'])
      if response.status_code is 200:
            content = json.loads(response.content.decode('utf-8')) # the whole nested content


# The data are not presented here as simple objects; the data are categorized into countries and their content in time series

from pandas.io.json import json_normalize
# features = json_normalize(content['features'])
# features = pd.DataFrame(json_normalize(content['features']))

stu_df = pd.DataFrame(json_normalize(content)) # Inversion of the values, country per column
col_names =  ['confirmed', 'date', 'country', 'deaths', 'recovered']
features  = pd.DataFrame(columns = col_names) # Bare structure for the normalized content

# Theoretically, one would not have to normalize the data for ElasticSearch, since object-oriented databases can use partially structured data. For types converting to pandas, however, this is easier. In addition, the existing Elastic Client does not have to be rewritten.

# Normalization of the data
for (columnName, columnData) in stu_df.iteritems(): 
#       print('Colunm Name : ', columnName)
      structured = pd.DataFrame(json_normalize(content[columnName]))
      # Using DataFrame.insert() to add a column 
      structured.insert(2, "country", columnName, True)
      structured['date'] = pd.to_datetime(structured['date']) # Convert to a date type
      list = ['confirmed', 'deaths', 'recovered'] # Columns that have to be converted to integers
      for type_c in list:
            structured[type_c] = structured[type_c].astype(int)
#       print(structured)
      features = features.append(structured) # Append the frames
print(features.head(6))

"""
#      confirmed       date   country  deaths  recovered
# 0            0 2020-01-22  Zimbabwe       0          0
# 1            0 2020-01-23  Zimbabwe       0          0
"""

# print(features.info())

from datetime import datetime

def safe_date(date_value):
      return (
            pd.to_datetime(date_value) if not pd.isna(date_value)
                  else  datetime(1970,1,1,0,0)
      )
      
features['date'] = features['date'].apply(safe_date)
# features.to_csv('/Users/asdf/Desktop/asdf.csv', index = False, encoding='utf-8-sig')


import requests
import json

url = 'http://localhost:9200/asdf?pretty'
body = {
  "settings": {
    "number_of_shards": 3,
    "number_of_replicas": 2
  }
}


# Test area for single-cell insertions
def insert_single():
      json_q = json.dumps({"asdf": int(12), "name" : "bob"})
      url = "http://localhost:9200/single-doc/_doc"
      headers ={'Content-Type' : 'application/json'}
      requests.post(url, headers = headers,  data = json_q)  # <-- specify content type
      print('single document inserted')
# insert_single()

def send_to_elastic(elastic_address, endpoint, to_elastic_string, count):
      headers ={'Content-Type' : 'application/json'}
      url = str(elastic_address + endpoint)
      try:
            catcher = requests.post(url, headers = headers, data = to_elastic_string)
#             print(catcher, "Inserted documents: " + str(int(count)))
      except:
            print('Error occurred in send_to_elastic!')


elastic_address = "http://localhost:9200"
headers ={'Content-Type' : 'application/json'}
endpoint = "/_bulk?pretty"
to_elastic_string = ""
target_index = "pomber"
target_type = "services.pomber"

# index the documents
count = 0
for (index, row) in features.iterrows():
      generated_key = str(row['date']).replace(' ', '_') + '-' + row['country'] # generates the artificial key
      index_row = { "index": { "_index" : target_index, "_type" : target_type, "_id" : str(generated_key)}}
      json_string = json.dumps(index_row) + "\n" + row.to_json(date_format='iso') + "\n"
      to_elastic_string += json_string
#       print(to_elastic_string)
      count += 1
      if count % 200 == 0:
            try:
                  send_to_elastic(elastic_address, endpoint, to_elastic_string, count)
                  to_elastic_string = ""
            except:
                  print('damn it, we have a problem')
# successfully inserted into Elasticsearch
print('documents were successfully inserted into elasticsearch.')
# if Response.status != 200:
#             print ("\n*** Error occured before import. ***")
#             print ("HTTP error code {} / {}".format(response.status, response.reason))
# elif Response.status == 200:
#             print("\n*** Data was transferred successfully. ***")

